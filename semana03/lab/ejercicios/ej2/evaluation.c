#include "prop.h"
#include "evaluation.h"


bool eval(prop_t prop, bool line[]) {
/* evaluates the proposition in the given line of the truth table
 * assumes array line has the values for all the propositional letters
 * in the proposition
 */
    bool res;
    if (is_prop_true(prop)) {
        res = true;
    } else if (is_prop_false(prop)) {
        res = false;
    } else if (is_prop_var(prop)) {
        res = line[get_var(prop)];
    } else if (is_prop_not(prop)) {
        res = !eval(get_sub_prop(prop),line);
    } else if (is_prop_and(prop)) {
        res = eval(get_left_prop(prop),line) && eval(get_right_prop(prop),line);
    } else if (is_prop_or(prop)) {
        res = eval(get_left_prop(prop),line) || eval(get_right_prop(prop),line);
    } else if (is_prop_then(prop)) {
        res = !eval(get_left_prop(prop),line) || eval(get_right_prop(prop),line);
    } else if (is_prop_iff(prop)) {
        res = eval(get_left_prop(prop),line) == eval(get_right_prop(prop),line);
    } 
    return (res);
}

static void next_line(bool line[], unsigned int length) {
/* changes line to become the next line of the truth table,
 *
 * needs implementation
 */
}


static void mk_fst_line(bool line[], unsigned int length) {
/* makes the first line of the truth table,
 * for instance false false false ... false
 *
 * needs implementation
 */
}

static bool last_line(bool line[], unsigned int length) {
/* returns true iff line is the first line of the truth table
 *
 * needs implementation
 */
}



bool is_tautology(prop_t prop) {
  /* determines if prop is a tautology
   * needs implementation
   */
}


