#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort_helpers.h"
#include "sort.h"

static void next_permutation(unsigned int perm[], unsigned int length) {
    unsigned int i, j;
    // taken (and modified) from
    // https://www.nayuki.io/page/next-lexicographical-permutation-algorithm 

    // Find non-increasing suffix
    i = length - 1;
    while (i > 0 && perm[i - 1] >= perm[i]) {
        i--;
    }
    
    // Find successor to pivot
    j = length - 1;
    while (perm[j] <= perm[i - 1]) {
        j--;
    }

    swap(perm, i - 1, j);
    
    // Reverse suffix
    j = length - 1;
    while (i < j) {
        swap(perm, i, j);
        i++;
        j--;
    }
}

static void mk_fst_permutation(unsigned int perm[], unsigned int length) {
/* makes the first permutation 0 1 2 ... length-1
 *
 * needs implementation
 */
}

static bool sorted(int a[], unsigned int perm[], unsigned int length) {
/* returns true iff the array a would be sorted following the order
 * indicated by perm
 *
 * (the ordering relation is determined by the abstract function goes_before)
 *
 * needs implementation
 */
}

static void update(int a[], unsigned int perm[], unsigned int length) {
/* resets a according to the order indicated in perm 
 *
 * needs implementation
 */
}

void permutation_sort(int a[], unsigned int length) {
/* tries systematically with all te possible permutations
 * until a sorted one is found
 */
    unsigned int perm[length];
    mk_fst_permutation(perm, length);
    while (!sorted(a, perm, length)) {
        next_permutation(perm, length);
    }
    update(a, perm, length);
}

